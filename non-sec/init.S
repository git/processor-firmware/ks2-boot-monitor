@******************************************************************************
@
@ init.S - Init code routines
@
@******************************************************************************
@
@ Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
@
@
@  Redistribution and use in source and binary forms, with or without
@  modification, are permitted provided that the following conditions
@  are met:
@
@    Redistributions of source code must retain the above copyright
@    notice, this list of conditions and the following disclaimer.
@
@    Redistributions in binary form must reproduce the above copyright
@    notice, this list of conditions and the following disclaimer in the
@    documentation and/or other materials provided with the
@    distribution.
@
@    Neither the name of Texas Instruments Incorporated nor the names of
@    its contributors may be used to endorse or promote products derived
@    from this software without specific prior written permission.
@
@  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
@  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
@  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
@  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
@  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
@  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
@  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
@  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
@  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
@  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
@  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
@
@******************************************************************************
#include "config.h"

@****************************** Global Symbols*********************************
			.align 5
			.global _start
			.global __bss_start__
			.global __bss_end__
			.global __stack
			.global _start_primary_cpu
			.global _fast_boot_ns_ep
			.global init_primary_cpu
			.global nonsec_init_addr
			.set nonsec_init_addr, IMAGE_LOAD_ADDR
			.set skern_init, IMAGE_LOAD_ADDR + NONSEC_IMAGE_SIZE

@**************************** Code Seection ***********************************
			.text

@
@ This code is assembled for ARM instructions
@
			.code 32

@******************************************************************************
@
@******************************************************************************
@
@ The reset handler in StarterWare is named as '_start'.
@ The reset handler sets up the stack pointers for all the modes. The FIQ and
@ IRQ shall be disabled during this. Then clear the BSS sections and finally
@ switch to the function calling the main() function.
@
_start:
_start_primary_cpu:
			stmfd sp!, {r0-r12, lr}
			bl      switch_hyp
			ldmfd sp!, {r0-r12, lr}

			stmfd r13!, {r4-r12, lr}
			bl	init_primary_cpu
			ldmfd r13!, {r4-r12, lr}
			mov pc, lr

			.set sup_stack_size,	1024
			.set sup_stack_start, 	__stack - 4096
			.set sup_stack0_addr, sup_stack_start + sup_stack_size
			.set sup_stack1_addr, sup_stack0_addr + sup_stack_size
			.set sup_stack2_addr, sup_stack1_addr + sup_stack_size
			.set sup_stack3_addr, sup_stack2_addr + sup_stack_size
			.set max_chip_id, 3
sup_stack0_top:	.word	sup_stack0_addr
sup_stack1_top:	.word	sup_stack1_addr
sup_stack2_top:	.word	sup_stack2_addr
sup_stack3_top:	.word	sup_stack3_addr

@*************************************************************************
	.align	5
_fast_boot_ns_ep:
			mov	r0, r0
			mov	r0, r0
@
@ we may not need to setup the stack, but in case we need we set it
@
			@Read the multi-processor affinity register
			mrc p15, #0, r0, c0, c0, #5
			and r0,  r0, #3		@The core number
			adr r2, sup_stack0_top
			lsl r0, #2
			ldr r3, [r2, r0]
			mov sp, r3

			mov     r8, #0
			mcr     p15, 0, r8, c8, c7, 0   @ invalidate TLBs
			mcr     p15, 0, r8, c7, c5, 0   @ invalidate icache
			mcr     p15, 0, r8, c7, c5, 6   @ invalidate BP array
			dsb
			isb

			mrc     p15, 0, r8, c1, c0, 0	@ disable I-cache
			bic     r8, r8, #0x1000
			mcr     p15, 0, r8, c1, c0, 0

			@r1 will have the secondary Linux kernel branch address
			mov pc, r1

@****************************************************************************
switch_hyp:
	@switch to hyp mode
	stmfd sp!, {lr}
	mov r1, #1

	@The SP register is a banked register and when the switch to hypervisor
	@occurs, it reflects the view of the hyp mode a seemingly "corrupt"
	@value of SP is expected. Hence save this.
	ldr r0, =save_sp
	str sp, [r0]
	ldr r0, =set_hyp
	smc #0
	ldr r0, =save_sp
	ldr sp, [r0]
	ldmfd sp!, {lr}
	bx lr

save_sp:       /* Storage for stack pointer */
       .word   0x0

set_hyp:
	@This function is called in Monitor mode save the link register
	stmfd sp!, {lr}

	@Read the SCR - Secure Configuration Register
	mrc p15,#0x0,r0,c1,c1,#0
	orr r0,r0,#0x100	@Set Hyp Call enable(HCE) Bit in SCR
	mcr p15,#0x0,r0,c1,c1,#0

	@Read the mode bits in spsr and change the mode to HYP mode = 0x1a
	mrs r0, spsr
	bic r0, r0, #0x1f
	orr r0, r0, #0x1a	@HYP MODE setting for cpu0
	msr spsr_cx, r0
	ldmfd sp!, {lr}
	mov pc, lr

init_primary_cpu:
			@ monitor_function saves all register context
			@ register context before invoking skern_init
			stmfd r13!, {r0-r3, lr}
			mov r3, r2
			mov r2, r1
			ldr r0, =skern_init
			mov r1, #1
			bl monitor_function
			isb
			ldmfd r13!, {r0-r3, lr}
			mov pc, lr

			.global monitor_function
@used only when RBL monitor vector ans svc vectors are in place
monitor_function:
			stmfd r13!, {lr}
			smc #0
			ldmfd r13!, {lr}
			bx lr

         .end
