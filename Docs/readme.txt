The Boot Monitor is an application that provides secure privileged services
to Linux kernel to execute functions in secure privileged mode. It is invoked
through the SMC ARM instruction.

The "skern-<SOC>" flat binary image is created by the build system that can be
loaded to MSCM through CCS.

The "skern-<SOC>.bin" file is a U-Boot loadable mkimage file that can be loaded
by U-Boot.

The Boot Monitor is installed and initialized through the
"install_mon <loaded address>" u-boot command.

To build boot monitor
======================

Need Linaro tool chain. Set your CROSS_COMPILE path as

export CROSS_COMPILE=arm-linux-gnueabihf-

Assume boot-monitor.git is cloned to your home directory.

To build boot monitor do

cd ~/boot-monitor
make clean
make

build will generate the "skern-<SOC>[.bin]" under cwd or a directory
specified by O=<dir> passed to make.
