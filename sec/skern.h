/*
 * Copyright (C) 2016-2017 Texas Instruments Incorporated - http://www.ti.com/
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __SKERN_H
#define __SKERN_H

struct regs {
	unsigned	psr;
	unsigned	r0;
	unsigned	r1;
	unsigned	r2;
	unsigned	r3;
	unsigned	r4;
	unsigned	r5;
	unsigned	r6;
	unsigned	r7;
	unsigned	r8;
	unsigned	r9;
	unsigned	r10;
	unsigned	r11;
	unsigned	r12;
	unsigned	lr;
};

/* MPU settings registers */
#define MPU_CFG_REG1		0x02389a48
#define MPU_CFG_REG2		0x02389ac8
#define MPU_CFG_REG3		0x02368208
#define MPU_CFG_REG4		0x02370208
#define MPU_CFG_REG5		0x02388208

/* MSMC SES registers */
#define K2G_MSMC_CFG_BASE       (0x0bc00000)
#define MSMC_SES_MPAXL(PID, N)  (0x600 + ((PID) * 8 + (N)) * 8)
#define MSMC_SES_MPAXH(PID, N)  (0x604 + ((PID) * 8 + (N)) * 8)
#define MSMC_SES_ALL            (0xff)
#define MSMC_SEGSZ_4KB          (0x0b)
#define K2G_MSMC_PRIVID_QM      (9)

/* DDR3A config base */
#define K2G_DDR3A_CFG_BASE      (0x21010000)

int skern_command(int cmd, const struct regs *regs);
void *skern_init(unsigned int (*fcn_p(void)), unsigned int from,
		 unsigned int dpsc_base, unsigned int freq);
void *skern_123_init(void);
int skern_poweron_cpu(int cpu_id, void *kern_ep);
int skern_poweroff_cpu(int cpu_id);

#endif	/* __SKERN_H */
