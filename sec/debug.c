/*
 * Copyright (C) 2013-2016 Texas Instruments Incorporated - http://www.ti.com/
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdarg.h>

#include "config.h"
#include "debug.h"

#ifndef UART_BASE
/* Use UART0 base address as default */
#define UART_BASE	0x02530c00
#endif

#ifdef DYNAMIC_DEBUG
static char _debug_print;
static inline char do_print(void) { return _debug_print; }
#else
static inline char do_print(void) { return 1; }
#endif

struct uart_regs {
	unsigned int rbr;		/* 0 */
	unsigned int ier;		/* 1 */
	unsigned int fcr;		/* 2 */
	unsigned int lcr;		/* 3 */
	unsigned int mcr;		/* 4 */
	unsigned int lsr;		/* 5 */
	unsigned int msr;		/* 6 */
	unsigned int spr;		/* 7 */
};

static struct uart_regs *uart0 = (struct uart_regs *)UART_BASE;

static inline void skern_putc(unsigned char c)
{
	while((uart0->lsr & 0x20) == 0);
	uart0->rbr = c;
}

static inline void skern_puts(char *str)
{
	for (; *str != '\0'; str++ )
		skern_putc(*str);
}

static char dig[] = "0123456789abcdef";
static inline void skern_putbyte(unsigned char b)
{
	skern_putc(dig[(b >> 4) & 0xf]);
	skern_putc(dig[b & 0xf]);
}

static void skern_putui(unsigned int ul)
{
	skern_putbyte((ul >> 24) & 0xff);
	skern_putbyte((ul >> 16) & 0xff);
	skern_putbyte((ul >>  8) & 0xff);
	skern_putbyte( ul        & 0xff);
}

static void skern_put_base(int d, int base)
{
	char buf[20], *p;

	p = buf + sizeof(buf) - 1;
	*p = 0;

	if (d < 0) {
		skern_putc('-');
		d = -d;
	}

	if (!d) {
		p--;
		*p = dig[0];
	}

	while (d) {
		p--;
		if (p == buf)
			break;
		*p = dig[d % base];
		d /= base;
	}
	skern_puts(p);
}

void skern_printf(char *fmt, ...)
{
	va_list ap;

	if (!do_print())
		return;

	va_start(ap, fmt);

	while (*fmt) {
		/* Convert \ns to contain \r */
		if (*fmt == '\n') {
			skern_puts("\n\r");

		}
		if (*fmt != '%') {
			skern_putc(*fmt);
			fmt++;
			continue;
		}

		fmt++; /* loose the '%' */

		switch (*fmt++) {
		case 's':	/* string */
			skern_puts(va_arg(ap, char *));
			break;
		case 'x':	/* hex */
			skern_putui(va_arg(ap, int));
			break;
		case 'o':	/* int */
			skern_put_base(va_arg(ap, int), 8);
			break;
		case 'd':	/* int */
			skern_put_base(va_arg(ap, int), 10);
			break;
		case 'c':	/* char */
			skern_putc((char)va_arg(ap, int));
			break;
		case '%':
			skern_putc(*fmt);
			break;
		case 0:
			return;
		}
	}
	va_end(ap);
}

#ifdef DYNAMIC_DEBUG
/**
 * skern_debug_command() - Do Debug SCI commands
 * @cmd:	command to process - ONLY deal with SCI commands
 * @regs:	ARM registers
 * @error:	PSCI error code to return
 *
 * Return: 0 if handled, else return 1
 */
int skern_debug_command(int cmd, int *error)
{
	switch (cmd) {
	case TI_SCI_DISABLE_DEBUG:
		_debug_print = 0;
		*error =  0;
		return 0;
	case TI_SCI_ENABLE_DEBUG:
		_debug_print = 1;
		*error =  0;
		return 0;
	}
	return 1;
}
#endif
