/*
 * Copyright (C) 2013-2016 Texas Instruments Incorporated - http://www.ti.com/
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __SKERN_DEBUG_H
#define __SKERN_DEBUG_H

/*
 * See http://infocenter.arm.com/help/topic/com.arm.doc.den0028a/
 * For information on SMC calling conventions. SIP implementation
 * For TI SoCs using PMMC follows:
 *
 * 0x0000 to 0x7FFF is defined for Power/SoC features
 * 0x8000 to 0xFFFF is defined for security features
 */
#define SIP_32_TI(VAL)		(0x82000000 | ((VAL) & 0x7FFF))

#define TI_SCI_ENABLE_DEBUG	SIP_32_TI(0x7000)
#define TI_SCI_DISABLE_DEBUG	SIP_32_TI(0x7001)

#ifndef NOUART
void skern_printf(char *fmt, ...);
#else
static inline void skern_printf(char *fmt, ...) { }
#endif

#if defined(DYNAMIC_DEBUG) && !defined (NOUART)
int skern_debug_command(int cmd, int *error);
#else
static inline int skern_debug_command(int cmd, int *error)
{
	return 1;
}
#endif

#ifdef DEBUG
#define skern_debug(ARGS...) skern_printf("DEBUG: "ARGS)
#else
#define skern_debug(ARGS...)
#endif

#endif /* __SKERN_DEBUG_H */
