/*
 * Copyright (C) 2013-2016 Texas Instruments Incorporated - http://www.ti.com/
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>

#include "config.h"
#include "skern.h"
#include "skernel.h"
#include "debug.h"
#include "psci.h"

#ifdef DEBUG
int mpu_dump[64];
#endif

/**
 * skern_psci_command() - Do PSCI commands
 * @cmd:	command to process - ONLY deal with PSCI commands
 * @regs:	ARM registers
 * @error:	PSCI error code to return
 *
 * Return: 0 if handled, else return 1
 */
int skern_psci_command(int cmd, const struct regs *regs, int *error)
{
#ifdef DEBUG
	int i;
	unsigned int mpu_reg;
#endif

	switch (cmd) {
	case 0:
	case PSCI_CPU_ON:
		*error =  skern_poweron_cpu(regs->r1,		/* cpu    */
					   (void *)regs->r2);	/* entry  */
		return 0;
	case 1:
		*error =  skern_poweroff_cpu(regs->r1);		/* cpu    */
		return 0;
#ifdef DEBUG
	case 2:
		mpu_reg = MPU_CFG_REG3;
		for (i = 0; i < 16; i++) {
			mpu_dump[i] = *(unsigned int *)mpu_reg;
			mpu_reg += 0x10;
		}
		mpu_reg = MPU_CFG_REG4;
		for (i = 16; i < 30; i++) {
			mpu_dump[i] = *(unsigned int *)mpu_reg;
			mpu_reg += 0x10;
		}
		mpu_reg = MPU_CFG_REG5;
		for (i = 30; i < 44; i++) {
			mpu_dump[i] = *(unsigned int *)mpu_reg;
			mpu_reg += 0x10;
		}
		mpu_dump[44]  = 0xdeadbeef;
		break;
#endif
	case PSCI_VERSIONS:
		*error = 0x00000002;
		return 0;

	case PSCI_SUSPEND:
		*error = PSCI_NOT_SUPPORTED;
		return 0;

	case PSCI_CPU_OFF:
		*error =  skern_poweroff_cpu(chip_get_arm_num());
		skern_printf("OOPS, PSCI_CPU_OFF DIDN'T WORK\n");
		*error = PSCI_INTERNAL_FAILURE;
		return 0;
	}

	return 1;
}
