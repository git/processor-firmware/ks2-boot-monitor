# /*
#  * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
#  *
#  *  Redistribution and use in source and binary forms, with or without
#  *  modification, are permitted provided that the following conditions
#  *  are met:
#  *
#  *    Redistributions of source code must retain the above copyright
#  *    notice, this list of conditions and the following disclaimer.
#  *
#  *    Redistributions in binary form must reproduce the above copyright
#  *    notice, this list of conditions and the following disclaimer in the
#  *    documentation and/or other materials provided with the
#  *    distribution.
#  *
#  *    Neither the name of Texas Instruments Incorporated nor the names of
#  *    its contributors may be used to endorse or promote products derived
#  *    from this software without specific prior written permission.
#  *
#  *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  *
#  */
ROOT=.
include makedefs

NONSECPADBIN = nonsec_pad.bin
SECPADBIN = s-kernel_pad.bin
IMAGE = skern

export BOOTROM_VERSION_NAME = K2_BM
export BOOTROM_VERSION_MAJOR = 15
export BOOTROM_VERSION_MINOR = 07

SOCS=$(shell cd soc;ls)
BUILD_SOCS=$(patsubst %,image_%,$(SOCS))
CLEAN_SOCS=$(patsubst %,cleanimg_%,$(SOCS))

.PHONY: all clean build_image_trigger build_image build_clean tags cscope.out

# Error out if mkimage is not available.
ifeq ($(shell which mkimage),)
$(error "No mkimage in $(PATH), consider installing u-boot-tools")
endif

all: $(BUILD_SOCS)

clean: $(CLEAN_SOCS)
	$(Q) rm -f cscope.out tags Tags ctags

cscope:
	@echo "Generating cscope database..."
	$(Q)$(CSCOPE) -R -b

tags:
	@echo "Generating tags..."
	$(Q)find . -iname "*.c" -o -iname "*.h" -o -iname "*.s" | xargs $(TAGS)

image_%:
	$(MAKE) build_image SOC=$(subst image_,,$@)

cleanimg_%:
	$(MAKE) build_clean SOC=$(subst cleanimg_,,$@)

build_image_trigger:
	$(Q)if [ -f $(IMAGE)-$(SOC).bin ] ; \
	then \
		$(MAKE) build_clean SOC=$(SOC); \
	fi

	$(MAKE) build_image SOC=$(SOC)

build_image:
	@echo "Building for SoC=$(SOC)"
	$(MAKE) SOC=$(SOC) -C sec
	$(MAKE) SOC=$(SOC) -C non-sec
	@echo "Generating $(IMAGE)-$(SOC)"
	$(Q)cat $(OBJROOT)/soc/$(SOC)/$(NONSECPADBIN) > $(OBJROOT)/$(IMAGE)-$(SOC)
	$(Q)cat $(OBJROOT)/soc/$(SOC)/$(SECPADBIN) >> $(OBJROOT)/$(IMAGE)-$(SOC)
	mkimage -A arm -T firmware -C none \
		-a $(IMAGE_LOAD_ADDR) -e $(IMAGE_LOAD_ADDR) \
		-n "Boot Monitor for $(SOC)" -d $(OBJROOT)/$(IMAGE)-$(SOC) \
		$(OBJROOT)/$(IMAGE)-$(SOC).bin

build_clean:
	@echo "Cleaning for SoC=$(SOC)"
	$(Q)rm -rf $(OBJROOT)/$(IMAGE)-$(SOC)*
	$(MAKE) SOC=$(SOC) -C non-sec clean
	$(MAKE) SOC=$(SOC) -C sec clean
